//
//  VolumeModel.m
//  Novel17kSDKDemo
//
//  Created by Ink on 2020/2/7.
//  Copyright © 2020 ChineseAll. All rights reserved.
//

#import "VolumeModel.h"

@implementation ChapterModel

@end

@implementation VolumeModel

+ (NSDictionary *)modelContainerPropertyGenericClass{
    return @{@"chapterList":[ChapterModel class]};
}

@end
