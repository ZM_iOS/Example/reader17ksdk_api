//
//  ChapterListViewController.h
//  Novel17kSDKDemo
//
//  Created by Ink on 2020/2/7.
//  Copyright © 2020 ChineseAll. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ChapterListViewController : UIViewController

@property (strong, nonatomic) NSArray<VolumeModel *> *volumeListArray;

@end

NS_ASSUME_NONNULL_END
