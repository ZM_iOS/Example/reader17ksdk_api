//
//  ChapterListViewController.m
//  Novel17kSDKDemo
//
//  Created by Ink on 2020/2/7.
//  Copyright © 2020 ChineseAll. All rights reserved.
//

#import "ChapterListViewController.h"

@interface ChapterListViewController ()

@property (weak, nonatomic) IBOutlet UITableView *listTableView;

@end

@implementation ChapterListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.listTableView registerClass:UITableViewCell.class forCellReuseIdentifier:NSStringFromClass(UITableViewCell.class)];
    self.listTableView.tableFooterView = UIView.new;
    [self.listTableView reloadData];
}


#pragma mark - UITableView DataSource & Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.volumeListArray.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    VolumeModel *model = self.volumeListArray[section];
    return model.chapterList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    VolumeModel *model = self.volumeListArray[indexPath.section];
    ChapterModel *chapter = model.chapterList[indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass(UITableViewCell.class) forIndexPath:indexPath];
    cell.textLabel.text = chapter.chapterName;
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    VolumeModel *model = self.volumeListArray[section];
    return model.volumeName;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    VolumeModel *model = self.volumeListArray[indexPath.section];
    ChapterModel *chapter = model.chapterList[indexPath.row];    
    [[Novel17kSDK shareInstance] chapterContentWithBookId:chapter.bookId chapterId:chapter.chapterId completion:^(id  _Nonnull responseData, NSError * _Nonnull error) {
        [RouterCenter openData:responseData ?: error.localizedDescription];
    }];
}
@end
