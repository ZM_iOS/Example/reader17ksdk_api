//
//  VolumeModel.h
//  Novel17kSDKDemo
//
//  Created by Ink on 2020/2/7.
//  Copyright © 2020 ChineseAll. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ChapterModel : NSObject

/// 章节名
@property (copy, nonatomic) NSString *chapterName;

@property (copy, nonatomic) NSString *updateDate;
@property (copy, nonatomic) NSString *isVIP;

/// 书籍ID
@property (assign, nonatomic) NSInteger bookId;
/// 章节ID
@property (assign, nonatomic) NSInteger chapterId;
@property (assign, nonatomic) NSInteger volumeCode;
/// 卷ID
@property (assign, nonatomic) NSInteger volumeId;
@property (assign, nonatomic) NSInteger chapterIndex;

@end

@interface VolumeModel : NSObject
/// 卷名
@property (copy, nonatomic) NSString *volumeName;
/// 章节列表
@property (strong, nonatomic) NSArray<ChapterModel *> *chapterList;
/// 书籍ID
@property (assign, nonatomic) NSInteger bookId;
/// 卷ID
@property (assign, nonatomic) NSInteger volumeId;

@end

NS_ASSUME_NONNULL_END
