//
//  RouterCenter.h
//  Novel17kSDKDemo
//
//  Created by Ink on 2020/2/7.
//  Copyright © 2020 ChineseAll. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface RouterCenter : NSObject

+ (void)configNavigationController:(UINavigationController *)navigationController;

+ (void)openBooklist:(NSArray *)bookArray;

+ (void)openBook:(BookModel *)book;

+ (void)openVolumeList:(NSArray *)volumeListArray;

+ (void)openData:(id)data;

@end

NS_ASSUME_NONNULL_END
