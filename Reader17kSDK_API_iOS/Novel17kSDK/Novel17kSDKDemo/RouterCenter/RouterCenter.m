//
//  RouterCenter.m
//  Novel17kSDKDemo
//
//  Created by Ink on 2020/2/7.
//  Copyright © 2020 ChineseAll. All rights reserved.
//

#import "RouterCenter.h"

static  UINavigationController  *_navigationController = nil;

@implementation RouterCenter

+ (void)configNavigationController:(UINavigationController *)navigationController {
    _navigationController = navigationController;
}

+ (void)openBooklist:(NSArray *)bookArray {
    if (bookArray.count <= 0) {
        return;
    }
    BooklistViewController *bookListVC = BooklistViewController.new;
    bookListVC.bookListArray = bookArray;
    [_navigationController pushViewController:bookListVC animated:true];
}

+ (void)openBook:(BookModel *)book {
    if (!book) {
        return;
    }
    BookInfoViewController *bookVC = BookInfoViewController.new;
    bookVC.bookModel = book;
    [_navigationController pushViewController:bookVC animated:true];
}

+ (void)openVolumeList:(NSArray *)volumeListArray {
    if (volumeListArray.count <= 0) {
        return;
    }
    ChapterListViewController *chapterVC = ChapterListViewController.new;
    chapterVC.volumeListArray = volumeListArray;
    [_navigationController pushViewController:chapterVC animated:true];
}

+ (void)openData:(id)data {
    if (!data) {
        return;
    }
    ContentViewController *contentVC = ContentViewController.new;
    contentVC.data = data;
    [_navigationController pushViewController:contentVC animated:true];
}

@end
