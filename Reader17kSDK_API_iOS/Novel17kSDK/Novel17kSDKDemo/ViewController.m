//
//  ViewController.m
//  Novel17kSDKDemo
//
//  Created by Ink on 2020/2/4.
//  Copyright © 2020 ChineseAll. All rights reserved.
//

#import "ViewController.h"


@interface ViewController ()

@property (weak, nonatomic) IBOutlet UITextView *textView;

@property (weak, nonatomic) IBOutlet UITextField *bookIdTF;
@property (weak, nonatomic) IBOutlet UITextField *chapterIdTF;
@property (weak, nonatomic) IBOutlet UITextField *volumeIdTF;

@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;

@property (weak, nonatomic) IBOutlet UITextField *appIDTF;
@property (weak, nonatomic) IBOutlet UITextField *secretTF;

@property (weak, nonatomic) NSURLSessionDataTask *task;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.datePicker.date = [NSDate.new dateByAddingDays:-90];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(endedit)];
    [self.view addGestureRecognizer:tap];
    [RouterCenter configNavigationController:self.navigationController];
}

// 修改 appID secret
- (IBAction)saveHandler {
    [[Novel17kSDK shareInstance] configAppid:self.appIDTF.text ?: @"1027" secret:self.appIDTF.text ?:@""];
}

// 获取书单 ...测试使用 以服务端对接书单为准
- (IBAction)bookListHandler {
    
    [self beginNewTask];
    WEAKSELF

    NSString *dateStr = [self.datePicker.date formattedDateWithFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    self.task = [[Novel17kSDK shareInstance] bookListWithLastUpdateDate:dateStr page:1 count:20 completion:^(id  _Nonnull responseData, NSError * _Nonnull error) {
        [weakSelf handler:responseData error:error];
        
        NSArray *bookArray = [NSArray yy_modelArrayWithClass:BookModel.class json:responseData[@"content"]];
        [RouterCenter openBooklist:bookArray];
    }];
}

// 书籍详情
- (IBAction)bookInfoHandler {
    
    [self beginNewTask];
    WEAKSELF
    NSInteger bookId = [self.bookIdTF.text integerValue];
    self.task = [[Novel17kSDK shareInstance] bookInfoWithBookId:bookId completion:^(id  _Nonnull responseData, NSError * _Nonnull error) {
        [weakSelf handler:responseData error:error];
        
        BookModel *book = [BookModel yy_modelWithJSON:responseData[@"content"]];
        [RouterCenter openBook:book];
    }];
}

// 书籍卷列表
- (IBAction)volumeListHandler {
    
    [self beginNewTask];
    WEAKSELF
    NSInteger bookId = [self.bookIdTF.text integerValue];
    self.task = [[Novel17kSDK shareInstance] volumeListWithBookId:bookId completion:^(id  _Nonnull responseData, NSError * _Nonnull error) {
        [weakSelf handler:responseData error:error];
    }];
}

// 书籍卷和章节列表
- (IBAction)volumnAndChapterListHandler {
    
    [self beginNewTask];
    WEAKSELF
    NSInteger bookId = [self.bookIdTF.text integerValue];
    self.task = [[Novel17kSDK shareInstance] volumnAndChapterListWithBookId:bookId completion:^(id  _Nonnull responseData, NSError * _Nonnull error) {
        [weakSelf handler:responseData error:error];
        
        NSArray *volumeListArray = [NSArray yy_modelArrayWithClass:VolumeModel.class json:responseData[@"content"]];
        [RouterCenter openVolumeList:volumeListArray];
    }];
}

// 书籍章节列表
- (IBAction)chapterListHandler {
    
    [self beginNewTask];
    WEAKSELF
    NSInteger volumeId = [self.volumeIdTF.text integerValue];
    self.task = [[Novel17kSDK shareInstance] chapterListWithVolumeId:volumeId completion:^(id  _Nonnull responseData, NSError * _Nonnull error) {
        [weakSelf handler:responseData error:error];
    }];
}

// 章节增量更新列表
- (IBAction)chapterIncListHandler {
    
    [self beginNewTask];
    
    WEAKSELF
    NSInteger bookId = [self.bookIdTF.text integerValue];
    NSString *dateStr = [self.datePicker.date formattedDateWithFormat:@"yyyy-MM-dd HH:mm:ss"];
    self.task = [[Novel17kSDK shareInstance] chapterIncListWithBookId:bookId lastUpdateDate:dateStr completion:^(id  _Nonnull responseData, NSError * _Nonnull error) {
        [weakSelf handler:responseData error:error];
    }];
}

// 获取章节内容
- (IBAction)chapterContentHandler {
    
    [self beginNewTask];
    WEAKSELF
    NSInteger bookId = [self.bookIdTF.text integerValue];
    NSInteger chapterId = [self.chapterIdTF.text integerValue];
    self.task = [[Novel17kSDK shareInstance] chapterContentWithBookId:bookId chapterId:chapterId completion:^(id  _Nonnull responseData, NSError * _Nonnull error) {
        [weakSelf handler:responseData error:error];
    }];
}

- (void)endedit {
    [self.view endEditing:true];
}

- (void)beginNewTask {
    [self endedit];
    if (self.task) {
        [self.task cancel];
        self.textView.text = @"取消请求";
    }
    self.textView.text = @"获取中...";
}

- (void)handler:(id)responseData error:(NSError *)error {
    if (error) {
        self.textView.text = error.localizedDescription;
    }
    else {
        self.textView.text = [NSString stringWithFormat:@"%@",responseData];
    }
}

@end
