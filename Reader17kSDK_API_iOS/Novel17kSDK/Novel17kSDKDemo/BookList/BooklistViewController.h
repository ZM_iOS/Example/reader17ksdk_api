//
//  BooklistViewController.h
//  Novel17kSDKDemo
//
//  Created by Ink on 2020/2/7.
//  Copyright © 2020 ChineseAll. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BooklistViewController : UIViewController

@property (strong, nonatomic) NSArray<BookModel *> *bookListArray;

@end

NS_ASSUME_NONNULL_END
