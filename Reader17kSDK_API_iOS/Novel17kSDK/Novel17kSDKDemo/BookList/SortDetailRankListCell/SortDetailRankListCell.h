//
//  SortDetailRankListCell.h
//  novelReader
//
//  Created by Ink on 2019/7/24.
//  Copyright © 2019 dennis. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SortDetailRankListCell : UITableViewCell

+ (UINib *)cellNib;
+ (NSString *)className;

- (void)render:(BookModel *)bookModel index:(NSInteger) index;

@end

NS_ASSUME_NONNULL_END
