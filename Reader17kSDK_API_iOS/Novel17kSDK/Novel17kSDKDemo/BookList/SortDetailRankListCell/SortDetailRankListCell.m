//
//  SortDetailRankListCell.m
//  novelReader
//
//  Created by Ink on 2019/7/24.
//  Copyright © 2019 dennis. All rights reserved.
//

#import "SortDetailRankListCell.h"

@interface SortDetailRankListCell ()

@property (weak, nonatomic) IBOutlet UILabel *rankIndexLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *descLabel;
@property (weak, nonatomic) IBOutlet UILabel *authorLabel;
@property (weak, nonatomic) IBOutlet UILabel *remarkLabel;

// 趋势
@property (weak, nonatomic) IBOutlet UIImageView *trendImageView;

@property (weak, nonatomic) IBOutlet UIImageView *coverImageView;

@property (weak, nonatomic) IBOutlet UIImageView *honorImageView;

@end

@implementation SortDetailRankListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)render:(BookModel *)bookModel index:(NSInteger) index{
    [self.coverImageView sd_setImageWithURL:[NSURL URLWithString:bookModel.coverImageUrl]];
    self.nameLabel.text   = bookModel.bookName ? : @"";
    self.descLabel.text = bookModel.introduction ? : @"";
    self.authorLabel.text = [NSString stringWithFormat:@"%@ ⋅ %@",bookModel.authorPenname, bookModel.categoryName];
    
    self.rankIndexLabel.text = [NSString stringWithFormat:@"%ld.",(long)index + 1];

    self.trendImageView.hidden = false;
    self.remarkLabel.hidden = true;
    self.honorImageView.hidden = true;
}

+ (UINib *)cellNib {
    return [UINib nibWithNibName:self.className bundle:nil];
}

+ (NSString *)className {
    return NSStringFromClass(self);
}

@end
