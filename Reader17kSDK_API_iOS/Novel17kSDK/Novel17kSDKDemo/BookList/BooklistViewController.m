//
//  BooklistViewController.m
//  Novel17kSDKDemo
//
//  Created by Ink on 2020/2/7.
//  Copyright © 2020 ChineseAll. All rights reserved.
//

#import "BooklistViewController.h"
#import "SortDetailRankListCell.h"

@interface BooklistViewController ()

@property (weak, nonatomic) IBOutlet UITableView *listTableView;

@end

@implementation BooklistViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.listTableView registerNib:SortDetailRankListCell.cellNib forCellReuseIdentifier:SortDetailRankListCell.className];
    self.listTableView.tableFooterView = UIView.new;
    self.listTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.listTableView reloadData];
}

#pragma mark - UITableView DataSource & Delegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.bookListArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 90;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SortDetailRankListCell *cell = [tableView dequeueReusableCellWithIdentifier:SortDetailRankListCell.className forIndexPath:indexPath];
    [cell render:self.bookListArray[indexPath.row] index:indexPath.row];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [RouterCenter openBook:self.bookListArray[indexPath.row]];
}


@end
