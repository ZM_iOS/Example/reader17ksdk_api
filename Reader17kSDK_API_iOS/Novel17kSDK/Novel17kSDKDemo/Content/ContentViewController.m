//
//  ContentViewController.m
//  Novel17kSDKDemo
//
//  Created by Ink on 2020/2/7.
//  Copyright © 2020 ChineseAll. All rights reserved.
//

#import "ContentViewController.h"

@interface ContentViewController ()

@property (weak, nonatomic) IBOutlet UITextView *textView;

@end

@implementation ContentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.textView.text = [NSString stringWithFormat:@"%@",self.data];
}

@end
