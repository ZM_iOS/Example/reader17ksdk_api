//
//  BookInfoViewController.m
//  Novel17kSDKDemo
//
//  Created by Ink on 2020/2/7.
//  Copyright © 2020 ChineseAll. All rights reserved.
//

#import "BookInfoViewController.h"

@interface BookInfoViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *coverImageView;
@property (weak, nonatomic) IBOutlet UILabel *bookNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *authorNameLabel;

@end

@implementation BookInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.coverImageView sd_setImageWithURL:[NSURL URLWithString:self.bookModel.coverImageUrl]];
    
    self.bookNameLabel.text = self.bookModel.bookName;
    self.authorNameLabel.text = self.bookModel.authorPenname;
}

- (IBAction)volumeListHandler {
    
    NSInteger bookId = self.bookModel.bookId;
    
    [[Novel17kSDK shareInstance] volumnAndChapterListWithBookId:bookId completion:^(id  _Nonnull responseData, NSError * _Nonnull error) {
        
        if (responseData) {
            NSArray *volumeListArray = [NSArray yy_modelArrayWithClass:VolumeModel.class json:responseData[@"content"]];
            [RouterCenter openVolumeList:volumeListArray];
        }
        else {
            [RouterCenter openData:error.localizedDescription];
        }
    }];
}

@end
