//
//  BookModel.h
//  Novel17kSDKDemo
//
//  Created by Ink on 2020/2/7.
//  Copyright © 2020 ChineseAll. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface BookModel : NSObject

/// 书名
@property (copy, nonatomic) NSString *bookName;
/// 书籍封面
@property (copy, nonatomic) NSString *coverImageUrl;
/// 简介
@property (copy, nonatomic) NSString *introduction;
/// 作者
@property (copy, nonatomic) NSString *authorPenname;


@property (copy, nonatomic) NSString *className;
@property (copy, nonatomic) NSString *categoryName;
@property (copy, nonatomic) NSString *lastUpdateChapterDate;
@property (copy, nonatomic) NSString *keyWord;
@property (copy, nonatomic) NSString *bookStatus;

/// 书籍ID
@property (assign, nonatomic) NSInteger bookId;
@property (assign, nonatomic) NSInteger wordCount;
@property (assign, nonatomic) NSInteger status;

@end

NS_ASSUME_NONNULL_END
